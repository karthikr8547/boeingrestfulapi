var express = require('express');
var controllerCDM = require('./../controllers/controllerCDM');
var dbControllerCDM = require('./../services/dbservice');

var routeCDM = express.Router();

/*routeCDM.route('')
.get(controllerCDM.listOfFiles)
.post(controllerCDM.uploadFiles)
.delete(controllerCDM.deleteFile);

routeCDM.route('/:id')
.get(controllerCDM.getFileByID)
.put(controllerCDM.updateFile);*/

routeCDM.route('/CDMFilesCRUD/publishFilesUI')
.get(controllerCDM.publishFilesUI);

//routeCDM.route('/rest/files/findFiles')
//.post(controllerCDM.findFiles);

routeCDM.route('/rest/files/findFiles')
.get(controllerCDM.findFiles);

routeCDM.route('/rest/files/id/:id')
.get(controllerCDM.findFileById);

routeCDM.route('/rest/files/publishFiles')
.post(controllerCDM.publishFiles);

routeCDM.route('/rest/files/getDownloadURL')
.get(controllerCDM.getDownloadURL);

routeCDM.route('/rest/files/id/:id/downloadInfo')
.get(controllerCDM.downloadInfo);

module.exports = routeCDM;