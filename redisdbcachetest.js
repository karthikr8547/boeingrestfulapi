/*var config = {
    db:{
        use:'mysql',
        mysql : {
            master:{
                host : "127.0.0.1",
                user : "root",
                password : "123456",
                port : "3306",
                database : 'test'
            },
            slaves:
            [
                {
                    host : "127.0.0.1",
                    user : "root",
                    password : "123456",
                    port : "3306",
                    database : 'test'
                },
                {
                    host : "127.0.0.1",
                    user : "root",
                    password : "123456",
                    port : "3306",
                    database : 'test'
                }
            ]
        }
    },
    // middleData:{ 
    // 	use :'mongodb', 
    // 	mongodb:{ 
    // 		host : "127.0.0.1", 
    // 		port:"27017", 
    // 	} 
    // }, 
    cache:{
        use:'redis',
        redis : 
        [
            {
                host : process.env.REDIS_HOST,
                port : process.env.REDIS_PORT,
                password : process.env.REDIS_PASS, 
                db : 0
            },
//            {
//                host : "127.0.0.1",
//                port : "6379",
                // password : "", 
 //               db : 0
//            },
        ]
    }
};*/

var sqlConfig = {
    userName: process.env.SQL_USER,
    password: process.env.SQL_PASS,
    server: process.env.SQL_HOST,
    options: {encrypt: true, database: 'boeingcdmdb'}
};

var dbCache = require('db-cache');
var DBCacheEngine = dbCache.DBCacheEngine;
var BaseModule = dbCache.BaseModule;
var enigine = DBCacheEngine.getEngine(sqlConfig);
class FileDetails extends BaseModule
{
    constructor(engine,cacheId,isCache)
    {
        super(engine,process.env.SQL_USER,cacheId,isCache);
    }
 
    getFileDetails(fileId)
    {
        var res = await this.mySelect('select * from user where uid=?',[fileId],60);
        return res;
    }
 
    updateUser(fileId)
    {
        var res = this.myUpdate('Update user SET name="zs1" where uid=?',[fileId]);
        return res;
    }
}
 
//you can use uid as cacheId 
var fileId = 1;
var fileDetails = new FileDetails(enigine,fileId,true);
// user.getUser(1); 
// user.getUser(1); 
// user.updateUser(1); 
fileDetails.getFileDetails(1)
.then((res)=>{
    console.log(res);
    //if you need end Connect 
    fileDetails.endConnect();
});