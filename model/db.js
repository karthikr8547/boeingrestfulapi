var Connection = require('tedious').Connection;
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;

module.exports = function () {
    'use strict';

    var db = function (config) {
        this.config = config;
    }

    db.prototype.findFiles = function (params, callback) {
        this.connect(function (connection) {
            var fileId = 3;
            var query = `SELECT * FROM FILE_DETAILS WHERE FILEID=@fileId`;

            var statuses = [];
            var request = new Request(query, function (err, rowCount) {
                if (err) {
                    throw err;
                }
                callback({hasStatus: 0 < rowCount, statuses: statuses});
            });

            request.on('row', function (columns) {
                var status = {};
                columns.forEach(function (c) {
                    status[c.metadata.colName] = c.value;
                });
                statuses.push(status);
            });

            request.addParameter('fileId', TYPES.Int, fileId);

            connection.execSql(request);
        });
    };  

    db.prototype.publishFiles = function (params, callback) {
        this.connect(function (connection) {
            var query = `INSERT into CDMMETADATATBLE(FILE_ID,FILE_NAME,FILE_PATH,FILE_SIZE,LAST_MODIFIED) VALUES (@fileID,@fileName,@filePath,@fileSize,@lastModified)`;
            var request = new Request(query, function (err, rowCount) {
                if (err) {
                    throw err;
                }
                callback({addedStatus: 0 < rowCount});
            });
            console.log('Filename: '+req.params.fileName);
            request.addParameter('fileID', TYPES.Int, 1);
            request.addParameter('fileName', TYPES.NVarChar, req.params.fileName);
            request.addParameter('filePath', TYPES.NVarChar, '%test%');
            request.addParameter('fileSize', TYPES.Int, req.params.fileSize);
            request.addParameter('lastModified', TYPES.SmallDateTime, new Date().getTime());

            connection.execSql(request);
        })
    };  

    db.prototype.getDownloadURL = function (params, callback) {
        
        
    }; 
    
    db.prototype.connect = function (command) {
        var connection = new Connection(this.config);
        connection.on('connect', function (err) {
            if (err) {
                throw err;
            }
            command(connection);
        });
    };
    return db;
}();