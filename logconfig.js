var winston = require('./node_modules/winston'),
expressWinston = require('./node_modules/express-winston');
require('./node_modules/winston-daily-rotate-file');

  var transport = new (winston.transports.DailyRotateFile)({
    filename: './logs/log',
    datePattern: 'yyyy-MM-dd.',
    prepend: true,
    level: process.env.ENV === 'development' ? 'debug' : 'info'
  });

  var log = expressWinston.errorLogger({
    transports: [
        transport
    ]
  });
  
module.exports = {
    log: log
};