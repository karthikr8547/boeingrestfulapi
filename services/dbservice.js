var db = require('./../model/db');
require('dotenv').config();

function prepareParameters(req) {
    var params = {};
    /*if (req.body) {
        params.email = req.body.user_email;
        params.name = req.body.user_name;
        params.password = req.body.user_password;
        params.status = req.body.status;
    }*/

    /*if (req.session && req.session.key) {
        params.userId = req.session.key['user_id'];
    }*/
    params.fileId = 3;
    return params;
}

function createDb() {
    var sqlConfig = {
        userName: process.env.SQL_USER,
        password: process.env.SQL_PASS,
        server: process.env.SQL_HOST,
        options: {encrypt: true, database: process.env.SQL_DB}
    };

    return new db(sqlConfig);
}

function selectQuery(sql, type) {
    var method = null;
    
    if (type === 'findFiles') {
        method = sql.findFiles.bind(sql);
    } else if (type === 'publishFiles') {
        method = sql.publishFiles.bind(sql);
    } else if (type === 'getDownloadURL') {
        method = sql.getDownloadURL.bind(sql);
    } 
    return method;
}

var queryDb = function(req, type, callback) {

    var sql = createDb();

    var method = selectQuery(sql, type);
    if (!method) {
        console.log(type);
        return;
    }

    var params = prepareParameters(req);
    method(params, callback);
}

module.exports = queryDb;