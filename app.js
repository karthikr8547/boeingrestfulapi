var express = require('express');
var bodyParser = require('body-parser');
var routeCDM = require('./routes/routeCDM');
var bodyParser = require('body-parser');
var expWinston = require('./logconfig');
require("setimmediate");
var soap = require('soap');
var redisCache = require('./cache/rediscacheclient');

var app = express();

app.use(bodyParser.json());

var port = process.env.PORT || 1337;

app.use('',routeCDM);

app.use(expWinston.log);

/*var helloworldservice = {
  Hello_Service: {
      Hello_Port: {
          sayHello: function (args,callback) {
              console.log('sayHello: '+JSON.stringify(args));
              callback({'greeting': 'Hello '+args.firstName});
          }
      }
  }
};*/

//var xml = require('fs').readFileSync('HelloService.wsdl', 'utf8');

//app.use(bodyParser.raw({type: function(){return true;}, limit: '5mb'}));
app.listen(port,function(){
  //soap.listen(app, '/wsdl', helloworldservice, xml);
  console.log('Server is running on port: ' + port);
});

module.exports = app;