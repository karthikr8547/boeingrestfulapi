var soap = require('soap');
//var url = 'http://example.com/wsdl?wsdl';
//var url = 'http://www.jeppesen.com/CloudServices';
var url = 'http://localhost:1337/wsdl?wsdl';
var args = {firstName: 'Everybody'};
soap.createClient(url, function(err, client) {
    client.sayHello(args, function(err, result) {
        console.log(result);
    });
});