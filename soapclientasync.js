var soap = require('soap');
//var url = 'http://example.com/wsdl?wsdl';
//var url = 'http://www.jeppesen.com/CloudServices';
var url = 'http://localhost:1337/wsdl?wsdl';
var args = {firstName: 'Everyone'};
soap.createClientAsync(url).then((client) => {
  return client.sayHello(args);
}).then((result) => {
  console.log(result);
});